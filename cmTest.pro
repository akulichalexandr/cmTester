
QT       += core gui dbus

greaterThan(QT_MAJOR_VERSION, 4): {
    QT += widgets
    LIBS += -ltelepathy-qt5 -ltelepathy-qt5-service
    LIBS += -lsimplecm-qt5
    INCLUDEPATH += /usr/include/telepathy-qt5
} else {
    LIBS += -ltelepathy-qt4 -ltelepathy-qt4-service
    LIBS += -lsimplecm-qt4
    INCLUDEPATH += /usr/include/telepathy-qt4
}

TARGET = cmTest
TEMPLATE = app


SOURCES += main.cpp\
    MainWindow.cpp \
    CContactsModel.cpp \
    CComboBoxDelegate.cpp

HEADERS += MainWindow.hpp \
    CContactsModel.hpp \
    CComboBoxDelegate.hpp

FORMS += MainWindow.ui
